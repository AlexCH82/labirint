# Сборка проекта labirint

# Компилятор
CC = g++

# Флаги компилятора
CFLAGS = -c -Wall

# Флаги линковщика
LDFLAGS =

# Исходные тексты
SRC_DIR = src
SOURCES = $(SRC_DIR)/main.cpp $(SRC_DIR)/labirint.cpp

# Объектные и выполняемые файлы
OBJECTS = $(SOURCES:.cpp=.o)

# Выполняемый файл
EXECUTABLE=bin/labirint

all: $(SOURCES) $(EXECUTABLE)
	
$(EXECUTABLE): $(OBJECTS) 
	$(CC) $(LDFLAGS) $(OBJECTS) -o $@

.cpp.o:
	$(CC) $(CFLAGS) $< -o $@

clean:
	rm -rf $(SRC_DIR)/*.o bin/labirint
