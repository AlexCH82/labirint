/*
 * labirint.cpp
 * 
 * Copyright 2020 Alex Chernous <alexch82@ya.ru>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

#include "labirint.h"

#include <fstream>

// Переменные для модуля labirint 
t_field field; // Поле лабиринта
t_pos border;  // Максимальные размеры лабиринта
t_pos current; // Текущее положение в лабиринте
t_item * back; // Пройденный путь

// Загрузка данных из файла
int Load (string fName)
{
	ifstream ifs(fName);
	if (ifs) {
		string buff;
		border.row = 0;
		border.col = 0;
		while ( !ifs.eof() ) {
			// Считывание строки
			getline(ifs, buff);
			if (buff.size())
				border.col = buff.size();
			// Запись символов в массив
			for (unsigned int i = 0; i < border.col; i++) {
				field[border.row][i] = buff[i];
				// Запись начального положения
				if ( buff[i] == CH_START ) {
					current.row = border.row;
					current.col = i;
				}
				// TODO: Добавить проверку наличия выхода из лабиринта и крайних стен
			}
			// Переход к следующей строке
			border.row++;
		}
		// Учёт последней пустой строки
		border.row--;
		ifs.close();
		// TODO Инициализация начальных параметров
		back = NULL;
		return 0;
	} else
		return -1;
};

// Сохранение найденного пути в файл
int Save (string fName)
{
	ofstream ofs(fName);
	if (ofs) {
		t_item * p = new t_item; 
		do {
			// Запись координат и освобождение памяти
			ofs << back->pos.row << ' ' << back->pos.col << endl;
			p = back;
			back = back->next;
			delete p;
			p = nullptr;
		} while (back->next != NULL);
		ofs.close();
		delete p;
		return 0;
	} else
		return -1;
};

// Определение свободного поля и шаг на него
int Step() {
	field[current.row][current.col] = CH_STEP;
	t_item * p = new t_item;
	p->pos = current;
	p->next = back;
	back = p;

	// Шаг влево
	if ( (current.col > 1) && (field[current.row][current.col - 1] == CH_FREE) ) {
		current.col--;
		return 0;
	}
	// Шаг вверх
	if ( (current.row > 1) && (field[current.row - 1][current.col] == CH_FREE) ) {
		current.row--;
		return 0;
	}
	// Шаг вправо
	if ( (current.col < border.col) && (field[current.row][current.col + 1] == CH_FREE) ) {
		current.col++;
		return 0;
	}
	// Шаг вниз
	if ( (current.row < border.row) && (field[current.row + 1][current.col] == CH_FREE) ) {
		current.row++;
		return 0;
	}
	
	// Если не сделан шаг, то остаться на месте и освободить память
	back = back->next;
	delete p;
	return 0;
};

// Возвращение на предыдущую ячейку 
int StepBack() {
	// Пометить текущую ячейку заблокированной, вернуться и освободить память
	field[current.row][current.col] = CH_BLOCK;
	current = back->pos;
	t_item * p = new t_item ( *back->next );
	back = back->next;
	delete p;
	return 0;
};

// Проверка, является ли текущая ячейка тупиком, учитывая пройденный путь
bool isBlock() {
	string buff;
	buff.append(1, field[current.row][current.col - 1]);
	buff.append(1, field[current.row - 1][current.col]);
	buff.append(1, field[current.row][current.col + 1]);
	buff.append(1, field[current.row + 1][current.col]);
	return buff.find(CH_FREE) == string::npos && buff.find(CH_FINISH) == string::npos;
};

// Проверка, есть ли среди соседей ячейки выход
bool isFinish() {
	string buff;
	buff.append(1, field[current.row][current.col - 1]);
	buff.append(1, field[current.row - 1][current.col]);
	buff.append(1, field[current.row][current.col + 1]);
	buff.append(1, field[current.row + 1][current.col]);
	return  buff.find(CH_FINISH) == string::npos;
};

