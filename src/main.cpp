/*
 * main.cpp
 * 
 * Copyright 2020 Alex Chernous <alexch82@ya.ru>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

#include "labirint.h"

#include <iostream>

int main(int argc, char **argv)
{
	string inFile(""), outFile(""); // Входной и выходной файлы
	// Анализ параметров командной строки 
	switch (argc) {
		case 2:
			inFile = argv[1];
			outFile = "output.txt";
			break;
		case 3:
			inFile = argv[1];
			outFile = argv[2];
			break;
		default:
			inFile = "input.txt";
			outFile = "output.txt";
	}
	
	// Загрузка данных из файла
	if ( Load( inFile ) < 0 ) {
		cout << "Ошибка загрузки файла. Выход из программы с кодом -1" << endl;
		return -1;
	};

	// Основная часть - алгоритм поиска выхода из лабиринта 
	do {
		Step();
		if ( isBlock() )
			StepBack();
	} while ( isFinish() );
	
	// Сохранение результата
	if ( Save( outFile ) < 0 ) {
		cout << "Ошибка сохранения файла. Выход из программы с кодом -2" << endl;
		return -2;
	};
	
	return 0;
}
