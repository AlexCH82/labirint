/*
 * labirint.h
 * 
 * Copyright 2020 Alex Chernous <alexch82@ya.ru>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

#ifndef __LABIRINT_H__
#define __LABIRINT_H__

#include <string>

using namespace std;

const unsigned int MAX = 128; // Максимальная длина/ширина лабиринта
const char CH_START  = '>'; // Символ входа в лабиринт (должен быть только один)
const char CH_FINISH = '<'; // Символ выхода из лабиринта (должен быть только один)
const char CH_BLOCK  = '#'; // Символ непроходимого участка (стена)
const char CH_FREE   = ' '; // Символ проходимого участка (свободно)
const char CH_STEP   = '*'; // Символ пройденного пути (путь от входа до выхода)

// Поле лабиринта
typedef char t_field [MAX][MAX];

// Соседние ячейки (верх, низ, право, лево)
typedef char t_buffer [4];

// Текущее положение в лабиринте
struct t_pos {
	unsigned int col = 0, row = 0;
};

// Т. н. "нить Ареадны" - очередь пройденных ячеек
struct t_item {
	t_pos pos;
	t_item * next;	
};

int Load (string fName);
int Save (string fName);

int Step();
int StepBack();
bool isBlock();
bool isFinish();

#endif // __LABIRINT_H__
